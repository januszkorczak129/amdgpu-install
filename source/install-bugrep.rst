##############
Reporting Bugs
##############

Bugs should be reported through your AMD Customer Engagement Representative.

If you downloaded a release archive directly from the AMD web site and do not
have an AMD Customer Engagement Representative, you may report bugs using the
`freedesktop.org Bugzilla <https://bugs.freedesktop.org/enter_bug.cgi>`_.
File bugs according to the following table:

+-------------+----------------------+------------------------+
| **Variant** | **Bugzilla Product** | **Bugzilla Component** |
+=============+======================+========================+
| All-Open    | DRI                  | DRM/AMDgpu             |
+-------------+----------------------+------------------------+
| Pro         | DRI                  | DRM/AMDgpu-pro         |
+-------------+----------------------+------------------------+

=======================
Generating a Bug Report
=======================

In order to facilitate efficient investigation, it's important to include as
much detail as possible with the bug report. Perform the following steps
before you file a bug report:

1. Create a destination folder for the report.

   .. tip:: If you gather several reports, identify them with the date and
	if needed add a number at the end to differentiate between your
	reports, as shown in the following example.

   .. code-block:: bash

	  $ LOGDIR=~/LOGS/20170913-1
	  $ mkdir -p $LOGDIR
	  $ cd $LOGDIR

2. Gather basic configuration details.

   .. code-block:: bash

	 $ sudo lshw -c cpu | grep product >> basic.conf
	 $ lspci -nn | grep "VGA\|Display" >> basic.conf
	 $ sudo dmidecode -t BIOS | grep Version >> basic.conf
	 $ lsb_release -sd >> basic.conf
	 $ uname -a >> basic.conf
	 $ dkms status | grep "amdgpu\|radeon" >> basic.conf

3. Create detailed log files by running the following commands:

   .. code-block:: bash

	 $ dkms status > dkms.status.log
	 $ lsmod | grep amdgpu > lsmod.amdgpu.log
	 $ dmesg > dmesg.log
	 $ cp /var/log/Xorg.0.log .
	 $ type -p dpkg && dpkg -l > package.log
	 $ type -p rpm && rpm -qa > package.log

4. (Optional) To facilitate in-depth analysis, you can run the
   following commands:

   .. code-block:: bash

     $ lspci -vnn > lspci.vnn.log
     $ lspci -nn > lspci.nn.log
     $ sudo dmidecode > dmidecode.log
     $ uname -a > uname.a.log
     $ lsinitramfs /boot/initrd.img-`uname -r` > lsinitramfs.log
     $ sudo lshw > lshw.log
     $ modinfo amdgpu > modinfo.amdgpu.log
     $ glxinfo > glxinfo.log

-----------

Capturing the Output of Your Commands to a File
===============================================

1. Start a script session to capture your commands to a log file.

   .. code-block:: bash

	$ script /tmp/install-log.txt

2. Run all commands you want to capture in the script (for example, install
    All-Open base driver).

   .. code-block:: bash

    $ ./amdgpu-install -y

3. End the capture session.

   .. code-block:: bash

    $ exit
